const CAT = [
  {
    catid: 1,
    catname: "ঈমান",
    catayath: 30,
  },
  {
    catid: 2,
    catname: "তাওহীদ",
    catayath: 22,
  },
  {
    catid: 3,
    catname: "জিহাদ",
    catayath: 50,
  },
  {
    catid: 4,
    catname: "শাহাদত",
    catayath: 13,
  },
  {
    catid: 5,
    catname: "দাওয়া",
    catayath: 21,
  },
  {
    catid: 6,
    catname: "বিজ্ঞান",
    catayath: 165,
  },
  {
    catid: 7,
    catname: "জ্যার্তিবিদ্যা",
    catayath: 82,
  },
  {
    catid: 8,
    catname: "জীববিদ্যা",
    catayath: 66,
  },
  {
    catid: 9,
    catname: "ভ্রুনবিদ্যা",
    catayath: 39,
  },
  {
    catid: 10,
    catname: "পানিচক্র",
    catayath: 12,
  },
  {
    catid: 11,
    catname: "পর্দা",
    catayath: 10,
  },
  {
    catid: 12,
    catname: "ব্যাভিচার",
    catayath: 12,
  },
  {
    catid: 13,
    catname: "প্রেম",
    catayath: 2,
  },
  {
    catid: 14,
    catname: "শিরক",
    catayath: 14,
  },
  {
    catid: 15,
    catname: "মুনাফিক",
    catayath: 15,
  },
  {
    catid: 16,
    catname: "ইসলামি আইন",
    catayath: 16,
  },
  {
    catid: 17,
    catname: "খিলাফত",
    catayath: 9,
  },
  {
    catid: 18,
    catname: "নারী",
    catayath: 30,
  },
  {
    catid: 19,
    catname: "রিসালাত",
    catayath: 49,
  },
  {
    catid: 20,
    catname: "নামাজ",
    catayath: 18,
  },
  {
    catid: 21,
    catname: "রোজা",
    catayath: 5,
  },
  {
    catid: 22,
    catname: "হাজ্জ",
    catayath: 6,
  },
  {
    catid: 23,
    catname: "জাকাত",
    catayath: 4,
  },
  {
    catid: 24,
    catname: "মুমিন",
    catayath: 11,
  },
  {
    catid: 25,
    catname: "কুরআন",
    catayath: 75,
  },
  {
    catid: 26,
    catname: "কাফির",
    catayath: 43,
  },
  {
    catid: 27,
    catname: "ইসলাম",
    catayath: 0,
  },
  {
    catid: 28,
    catname: "সুদ",
    catayath: 2,
  },
  {
    catid: 29,
    catname: "ঘুষ",
    catayath: 1,
  },
  {
    catid: 30,
    catname: "মদ",
    catayath: 2,
  },
  {
    catid: 31,
    catname: "সালাম",
    catayath: 0,
  },
  {
    catid: 32,
    catname: "বিবাহ",
    catayath: 27,
  },
  {
    catid: 33,
    catname: "ধর্মনীরপেক্ষ মতবাদ",
    catayath: 8,
  },
  {
    catid: 34,
    catname: "ইসলামিক শাসনতন্ত্র",
    catayath: 7,
  },
  {
    catid: 35,
    catname: "অর্থনৈতিক ব্যাবস্থা",
    catayath: 6,
  },
  {
    catid: 36,
    catname: "যিকর ও প্রশংসা",
    catayath: 21,
  },
  {
    catid: 37,
    catname: "দুআ ও মোনাজাত",
    catayath: 48,
  },
  {
    catid: 38,
    catname: "হালাল",
    catayath: 14,
  },
  {
    catid: 39,
    catname: "হারাম",
    catayath: 42,
  },
  {
    catid: 40,
    catname: "জান্নাত",
    catayath: 20,
  },
  {
    catid: 41,
    catname: "জাহান্নাম",
    catayath: 33,
  },
  {
    catid: 42,
    catname: "কেয়ামত",
    catayath: 55,
  },
  {
    catid: 43,
    catname: "ইতিহাস / প্রত্নতত্ত্‌ব",
    catayath: 22,
  },
  {
    catid: 44,
    catname: "পদার্থবিদ্যা",
    catayath: 1,
  },
  {
    catid: 45,
    catname: "শরীয়ত আইন",
    catayath: 15,
  },
  {
    catid: 46,
    catname: "আহলে কিতাব (ইহুদী-খৃষ্টান)",
    catayath: 11,
  },
  {
    catid: 47,
    catname: "ফেরেস্তা",
    catayath: 6,
  },
  {
    catid: 48,
    catname: "আল্লাহ",
    catayath: 1,
  },
  {
    catid: 49,
    catname: "জ্বিন",
    catayath: 21,
  },
];

export default CAT;
