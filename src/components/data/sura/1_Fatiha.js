import React, { Component } from "react";
import { ThemeProvider, createTheme } from "@material-ui/core";
import AudioPlayer from "material-ui-audio-player";

const muiTheme = createTheme({});

export default class Fatiha extends Component {
  render() {
    const auto = {
      width: "280px",
    };
    return (
      <div className="container-fluid topbarpt">
        <div className="row justify-content-center">
          <div className="mb-5 mt-4">
            <ThemeProvider theme={muiTheme}>
              <AudioPlayer
                src="https://files.quantummethod.org.bd/media/audio/quran/001_al_fatihah.mp3"
                elevation={5}
                style={auto}
                variation="secondary"
                debug={false}
                displaySlider={true}
                volume={true}
                loop={true}
                spacing={2}
              />
            </ThemeProvider>
          </div>
        </div>

        <div className="card shadow mb-4">
          <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 className="m-0 font-weight-bold text-primary">আল ফাতিহা</h6>
          </div>
          <div className="card-body p-0">
            <div className="table-responsive">
              <table className="table table-striped table-hover text-dark">
                <tbody>
                  <tr>
                    <th>#</th>
                    <th>Ayat</th>
                    <td className="en-aya">English</td>
                  </tr>

                  <tr id="1">
                    <td>1</td>
                    <td colSpan="2" className="pd20 ar">
                      بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">
                      শুরু করছি আল্লাহর নামে যিনি পরম করুণাময়, অতি দয়ালু।
                    </td>
                    <td className="en-aya">
                      In the name of Allah, Most Gracious, Most Merciful.
                    </td>
                  </tr>
                  <tr id="2">
                    <td>2</td>
                    <td colSpan="2" className="pd20 ar">
                      الْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">
                      যাবতীয় প্রশংসা আল্লাহ তাআলার যিনি সকল সৃষ্টি জগতের
                      পালনকর্তা।
                    </td>
                    <td className="en-aya">
                      Praise be to Allah, the Cherisher and Sustainer of the
                      worlds;
                    </td>
                  </tr>
                  <tr id="3">
                    <td>3</td>
                    <td colSpan="2" className="pd20 ar">
                      الرَّحْمَٰنِ الرَّحِيمِ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">
                      যিনি নিতান্ত মেহেরবান ও দয়ালু।
                    </td>
                    <td className="en-aya">Most Gracious, Most Merciful;</td>
                  </tr>
                  <tr id="4">
                    <td>4</td>
                    <td colSpan="2" className="pd20 ar">
                      مَالِكِ يَوْمِ الدِّينِ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">যিনি বিচার দিনের মালিক।</td>
                    <td className="en-aya">Master of the Day of Judgment.</td>
                  </tr>
                  <tr id="5">
                    <td>5</td>
                    <td colSpan="2" className="pd20 ar">
                      إِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">
                      আমরা একমাত্র তোমারই ইবাদত করি এবং শুধুমাত্র তোমারই
                      সাহায্য প্রার্থনা করি।
                    </td>
                    <td className="en-aya">
                      Thee do we worship, and Thine aid we seek.
                    </td>
                  </tr>
                  <tr id="6">
                    <td>6</td>
                    <td colSpan="2" className="pd20 ar">
                      اهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">আমাদেরকে সরল পথ দেখাও,</td>
                    <td className="en-aya">Show us the straight way,</td>
                  </tr>
                  <tr id="7">
                    <td>7</td>
                    <td colSpan="2" className="pd20 ar">
                      صِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ
                      الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td className="bn bn-aya">
                      সে সমস্ত লোকের পথ, যাদেরকে তুমি নেয়ামত দান করেছ। তাদের পথ
                      নয়, যাদের প্রতি তোমার গজব নাযিল হয়েছে এবং যারা পথভ্রষ্ট
                      হয়েছে।
                    </td>

                    <td className="en-aya">
                      The way of those on whom Thou hast bestowed Thy Grace,
                      those whose (portion) is not wrath, and who go not astray.
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {/* <!-- Content Row --> */}
      </div>
    );
  }
}
