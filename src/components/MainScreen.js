import React from "react";
import Sidebar from "./pages/Sidebar";
import Main from "./pages/Main";

function MainScreen() {
  return (
    <div id="wrapper">
      <Sidebar />
      <Main />
    </div>
  );
}

export default MainScreen;
