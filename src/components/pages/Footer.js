import React from "react";

function Footer() {
  return (
    <footer className="sticky-footer bg-white">
      <div className="container my-auto">
        <div className="copyright text-center my-auto">
          <span>Copyright &copy; AlQuran 2022 </span>
          <span>
            Designed By &hearts; <a href="http://wapborhan.com">Borhan Uddin</a>
          </span>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
