import React, { Component } from "react";
import Alert from "../Extra/Alert";
import { FiBook, FiList, FiUsers } from "react-icons/fi";
import AyathList from "../../Listing/AyathList";

export default class AyathMain extends Component {
  render() {
    return (
      <div className="container-fluid topbarpt">
        <div className="row">
          <div className="col-lg-12 text-center">
            <Alert />
          </div>
        </div>
        {/* <!-- Page Heading --> */}
        {/* <!-- Content Row --> */}
        <div className="row">
          {/* <!-- Earnings (Monthly) Card Example --> */}
          <div className="col-xl-3 col-md-6 mb-4">
            <div className="card border-left-primary shadow h-100 py-2">
              <div className="card-body">
                <div className="row no-gutters align-items-center">
                  <div className="col mr-2">
                    <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                      মোট সূরা
                    </div>
                    <div className="h5 mb-0 font-weight-bold text-gray-800">
                      ১১৪ টি
                    </div>
                  </div>
                  <div className="col-auto">
                    <i className="text-gray-300 h3">
                      <FiBook />
                    </i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Earnings (Monthly) Card Example --> */}
          <div className="col-xl-3 col-md-6 mb-4">
            <div className="card border-left-success shadow h-100 py-2">
              <div className="card-body">
                <div className="row no-gutters align-items-center">
                  <div className="col mr-2">
                    <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                      পারা
                    </div>
                    <div className="h5 mb-0 font-weight-bold text-gray-800">
                      ৩০ টি
                    </div>
                  </div>
                  <div className="col-auto">
                    <i className="text-gray-300 h3">
                      <FiList />
                    </i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Earnings (Monthly) Card Example --> */}
          <div className="col-xl-3 col-md-6 mb-4">
            <div className="card border-left-success shadow h-100 py-2">
              <div className="card-body">
                <div className="row no-gutters align-items-center">
                  <div className="col mr-2">
                    <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                      আয়াত
                    </div>
                    <div className="h5 mb-0 font-weight-bold text-gray-800">
                      মতান্তরে ৬৩৪৮টি
                      <br /> অথবা ৬৬৬৬টি
                    </div>
                  </div>
                  <div className="col-auto">
                    <i className="text-gray-300 h3">
                      <FiList />
                    </i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Pending Requests Card Example --> */}
          <div className="col-xl-3 col-md-6 mb-4">
            <div className="card border-left-warning shadow h-100 py-2">
              <div className="card-body">
                <div className="row no-gutters align-items-center">
                  <div className="col mr-2">
                    <div className="text-xs font-weight-bold text-warning text-uppercase mb-1">
                      মানজিল
                    </div>
                    <div className="h5 mb-0 font-weight-bold text-gray-800">
                      ৭ টি
                    </div>
                  </div>
                  <div className="col-auto">
                    <i className="text-gray-300 h3">
                      <FiUsers />
                    </i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <!-- Content Row --> */}

        <div className="row">
          {/* SuraLish */}
          <div className="col-xl-12 col-lg-12">
            <AyathList />
          </div>
        </div>
        {/* <!-- Content Row --> */}
      </div>
    );
  }
}
