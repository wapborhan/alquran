import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Routes, Route } from "react-router";
import SuraDetails from "./SuraDetails";

const Sura = (props) => {
  const [suras, setPosts] = useState([]);
  useEffect(() => {
    axios
      .get("https://alquranbd.com/api/tafheem/suraData/1/1")
      .then((res) => {
        setPosts(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let sursFetch = suras.map((sura) => (
    <SuraDetails
      id={sura.id}
      suraID={sura.surah_id}
      ayaNum={sura.ayah_no}
      detailsSura={sura.ayah_text}
      bangla={sura.bn}
    />
  ));
  return (
    <>
      <tr>
        <th scope="row">{props.id}</th>
        <td>
          <Link to={`/sura/${props.id}/1`}>{props.sura}</Link>
        </td>
        <td>{props.ruku}</td>
        <td>{props.ayath}</td>
        <td>{props.obotoron}</td>
      </tr>

      <Routes>
        <Route
          exact
          path="sura/1/1"
          element={<SuraDetails detailsSura={sursFetch} />}
        />
      </Routes>
    </>
  );
};

export default Sura;
