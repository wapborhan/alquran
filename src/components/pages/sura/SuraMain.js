import React, { Component } from "react";

import SURAS from "../../data/sura/SuraName";
import SuraList from "./SuraList";

export default class SuraMain extends Component {
  // state
  state = {
    suras: SURAS,
    selectedSura: null,
  };

  selectedSuraHandler = (suraId) => {
    const sura = this.state.books.filter((sura) => sura.id === suraId)[0];
    this.setState({
      selectedSura: sura,
    });
  };

  render() {
    let suras = (
      <SuraList
        sura={this.state.suras}
        selectSura={this.state.selectedSuraHandler}
      />
    );

    return (
      <div className="row">
        <div className="col-xl-12 col-lg-12">
          <div className="card shadow mb-4">
            {/* <!-- Card Header - Dropdown --> */}
            <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 className="m-0 font-weight-bold text-primary">সূরা তালিকা</h6>
            </div>
            {/* <!-- Card Body --> */}
            <div className="card-body p-0">
              <div className="table-responsive">
                <table className="table table-bordered table-hover">
                  <thead className="thead-dark">
                    <tr>
                      <th scope="col" className="mobile-hide">
                        ক্রঃ
                      </th>
                      <th scope="col">সূরা নাম</th>
                      <th scope="col">রুকু</th>
                      <th scope="col">আয়াত</th>
                      <th scope="col">অবতীর্ণ </th>
                    </tr>
                  </thead>
                  <tbody>{suras}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
