import React from "react";
import Sura from "./Sura";

const SuraList = (props) => {
  return props.sura.map((sura, index) => {
    return (
      <Sura
        key={sura.id}
        id={sura.id}
        sura={sura.name}
        selectSura={() => {
          props.selectSura(sura.id);
        }}
      />
    );
  });
};

export default SuraList;
