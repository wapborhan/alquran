import AboutProject from "./AboutProject";
import AboutUS from "./AboutUS";
import ContactUS from "./ContactUS";
import AppAndroid from "./AppAndroid";
import NotFound from "./NotFound";

export { AboutProject, AboutUS, ContactUS, AppAndroid, NotFound };
