import React, { Component } from "react";

export default class Alert extends Component {
  render() {
    return (
      <div className="alert alert-danger" role="alert">
        ওয়েবসাইট এর কাজ চলছে, সব ফাংশন কাজ নাউ করতে পারে। মন্তব্য করতে{" "}
        <a
          href="https://www.facebook.com/photo?fbid=334038498731125&set=a.125115496290094"
          className="alert-link"
        >
          এখানে যান
        </a>
      </div>
    );
  }
}
