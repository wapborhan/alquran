import React, { Component } from "react";

export default class NotFound extends Component {
  render() {
    return (
      <div className="ntmain">
        <div className="mtimg">
          <div className="imgs text-center">
            <img src="/404.png" alt="404 Not Found" />
          </div>
          <div className="text-justify pt-5">
            <h1>
              Maybe this page moved? Got deleted? Is hiding out in quarantine?
              Never existed in the first place?
            </h1>
            <h1 className="p-3">
              Let's go <a href="/">home</a> and try from there.
            </h1>
          </div>
        </div>
      </div>
    );
  }
}
