import React from "react";
import {
  BsFillShareFill,
  BsListStars,
  BsListTask,
  BsFillPersonLinesFill,
  BsCodeSlash,
} from "react-icons/bs";
import { FiBook, FiHome, FiMail } from "react-icons/fi";
import { AiFillAndroid } from "react-icons/ai";
import { NavLink } from "react-router-dom";

function Sidebar() {
  return (
    <div className="sidebars">
      <div className="logo-details">
        <i>
          <FiBook />
        </i>
        <span className="logo_name">AlQuran</span>
      </div>

      <ul className="nav-links">
        <li>
          <NavLink to="/" className="activez">
            <i>
              <FiHome />
            </i>
            <span className="links_name">Home</span>
          </NavLink>
        </li>
        {/* <li>
          <NavLink to="/sura">
            <i className="text-white">
              <BsListTask />
            </i>
            <span className="links_name">Sura</span>
          </NavLink>
        </li> */}
        {/* <li>
          <NavLink to="/categories">
            <i className="text-white">
              <BsListStars />
            </i>
            <span className="links_name">Ayat</span>
          </NavLink>
        </li> */}
        {/* <li className="text-light border-top border-bottom d-flex  align-items-center justify-content-center">
          About
        </li> */}
        <li>
          <NavLink to="/about_project">
            <i className="text-white">
              <BsCodeSlash />
            </i>
            <span className="links_name">About Project</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/about_us">
            <i className="text-white">
              <BsFillPersonLinesFill />
            </i>
            <span className="links_name">About US</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/contact_us">
            <i className="text-white">
              <FiMail />
            </i>
            <span className="links_name">Contact US</span>
          </NavLink>
        </li>
        {/* <li className="text-light border-top border-bottom d-flex  align-items-center justify-content-center">
          Apps
        </li> */}
        <li>
          <NavLink to="/apps_android">
            <i className="text-white">
              <AiFillAndroid />
            </i>
            <span className="links_name">Android Apps</span>
          </NavLink>
        </li>
        <li className="log_out">
          <NavLink to="/sharer">
            <i className="text-white">
              <BsFillShareFill />
            </i>
            <span className="links_name">Share Your Friend</span>
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Sidebar;
