import React from "react";
import { Routes, Route } from "react-router";
import Header from "./Header";
import Home from "./Home";
import Footer from "./Footer";
import {
  AboutProject,
  AboutUS,
  ContactUS,
  AppAndroid,
  NotFound,
} from "./Extra/Extra";

function Main() {
  return (
    <div id="content-wrapper" className="d-flex flex-column">
      <div id="content">
        <Header />
        <Routes>
          <Route exact path="/" element={<Home />} />

          <Route exact path="/about_project" element={<AboutProject />} />
          <Route exact path="/about_us" element={<AboutUS />} />
          <Route exact path="/contact_us" element={<ContactUS />} />
          <Route exact path="/apps_android" element={<AppAndroid />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>
      <Footer />
    </div>
  );
}

export default Main;
