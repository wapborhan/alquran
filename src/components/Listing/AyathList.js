import React, { Component } from "react";
import CAT from "../data/AyathDataList";
import AyatShowhList from "../Listing/AyathShowList";
import { BsThreeDotsVertical } from "react-icons/bs";

class AyathList extends Component {
  state = {
    cat: CAT,
  };
  render() {
    const cats = this.state.cat.map((item) => {
      return <AyatShowhList cat={item} key={item.catid} />;
    });

    return (
      <div className="card shadow mb-4">
        {/* <!-- Card Header - Dropdown --> */}
        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 className="m-0 font-weight-bold text-primary">
            আয়াতের দ্বারা করা লিস্ট
          </h6>
          <div className="dropdown no-arrow">
            <a
              className="dropdown-toggle"
              href="#"
              role="button"
              id="dropdownMenuLink"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <i className="fas fa-ellipsis-v fa-sm fa-fw text-gray-400">
                <BsThreeDotsVertical />
              </i>
            </a>
            <div
              className="dropdown-menu dropdown-menu-right shadow animated--fade-in"
              aria-labelledby="dropdownMenuLink"
            >
              <div className="dropdown-header">Dropdown Header:</div>
              <a className="dropdown-item" href="#">
                Action
              </a>
              <a className="dropdown-item" href="#">
                Another action
              </a>
              <div className="dropdown-divider"></div>
              <a className="dropdown-item" href="#">
                Something else here
              </a>
            </div>
          </div>
        </div>
        {/* <!-- Card Body --> */}
        <div className="card-body p-0">
          <div className="table-responsive">
            <table className="table table-bordered table-hover">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">ক্রঃ</th>
                  <th scope="col">তালিকার নাম</th>
                  <th scope="col">মোট আয়াত</th>
                </tr>
              </thead>
              <tbody>{cats}</tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
export default AyathList;
