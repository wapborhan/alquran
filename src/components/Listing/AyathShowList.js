import React from "react";

const AyatShowhList = (props) => {
  return (
    <tr>
      <th scope="row">{props.cat.catid}</th>
      <td> {props.cat.catname}</td>
      <td> {props.cat.catayath}</td>
    </tr>
  );
};

export default AyatShowhList;
