import React from "react";
import "./assets/stylesheet/custom.css";
import "./assets/stylesheet/main.min.css";
import "./assets/javascript/main";
import MainScreen from "./components/MainScreen";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <MainScreen />
    </BrowserRouter>
  );
}

export default App;
